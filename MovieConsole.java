import java.util.Arrays;
import java.util.Scanner;

public class MovieConsole {
    public static void main(String[] args) {

        String[] movies = new String[3];
        double[] ratings = new double[3];
        int i = 0;
        while (i != 5) {
            showMenu();
            Scanner sc = new Scanner(System.in);
            int input = sc.nextInt();
            if (input == 1) {
                addThreeMovies(movies, ratings);
            } else if (input == 2) {
                if (movies[0] != null) {
                    showMoviesWithratings(movies, ratings);
                } else {
                    System.out.println("There is no movie");
                }
            } else if (input == 3) {
                System.out.println("How many movies do you want to add?: ");
                Scanner s = new Scanner(System.in);
                int inp = s.nextInt();
                addManyMovies(movies, ratings, inp);

            } else if (input == 4) {
                if (movies[0] != null) {
                    double avg = 0;
                    double sum = 0;
                    for (int a = 0; a < ratings.length; a++) {
                        sum += ratings[a];
                    }
                    avg = sum / ratings.length;
                    System.out.println("Average score of all movies: " + avg);
                    sortMovies(movies, ratings);
                    System.out.println("Movie with the max score: " + movies[movies.length - 1]);
                    System.out.println("Movie with the min score: " + movies[0]);
                } else {
                    System.out.println("There is no movie");
                }
            } else if (input == 5) {
                if (movies[0] != null) {
                    System.out.println("Enter the movie name: ");
                    Scanner scan = new Scanner(System.in);
                    String movieName = scan.nextLine();
                    searchByName(movies, ratings, movieName);
                } else {
                    System.out.println("There is no movie");
                }
            } else if (input == 6) {
                if (movies[0] != null) {
                    System.out.println("Enter the movie name: ");
                    Scanner scan = new Scanner(System.in);
                    String movieName = scan.nextLine();
                    updateRating(movies, ratings, movieName);
                } else {
                    System.out.println("There is no movie");
                }
            } else if (input == 7) {
                if (movies[0] != null) {
                    sortMoviesbySelection(movies, ratings);
                    showMovies(movies);
                } else {
                    System.out.println("There is no movie");
                }
            } else {
                i = 5;
            }
        }
    }

    public static void showMovies(String[] movies) {
        for (int r = 0; r < movies.length; r++) {
            System.out.println(r + 1 + ". " + movies[r]);
        }
    }

    public static void sortMoviesbySelection(String[] movies, double[] ratings) {
        double tmp;
        String tmp2;
        int min;
        for (int c = 0; c < ratings.length - 1; c++) {
            min = c;
            for (int j = c; j < ratings.length; j++) {
                if (ratings[j] < ratings[min]) {
                    min = j;
                }
            }
            tmp = ratings[c];
            ratings[c] = ratings[min];
            ratings[min] = tmp;
            tmp2 = movies[c];
            movies[c] = movies[min];
            movies[min] = tmp2;
        }
    }

    public static void sortMovies(String[] movies, double[] ratings) {
        double temp = 0;
        String temp2 = "";
        for (int b = 0; b < ratings.length; b++) {
            for (int j = b + 1; j < ratings.length; j++) {
                if (ratings[b] > ratings[j]) {
                    temp = ratings[b];
                    ratings[b] = ratings[j];
                    ratings[j] = temp;
                    temp2 = movies[b];
                    movies[b] = movies[j];
                    movies[j] = temp2;
                }
            }
        }
    }

    public static void updateRating(String[] movies, double[] ratings, String movieName) {
        for (int q = 0; q < movies.length; q++) {
            if (movies[q].equals(movieName)) {
                System.out.println("Enter new rating: ");
                Scanner scanner = new Scanner(System.in);
                double newRating = scanner.nextDouble();
                ratings[q] = newRating;
                break;
            } else if (q == movies.length - 1) {
                System.out.println("Movie not found");
            }
        }
    }

    public static void showMoviesWithratings(String[] movies, double[] ratings) {
        for (int n = 0; n < 3; n++) {
            System.out.print(n + 1 + ".movie: ");
            System.out.print(movies[n] + "\n");
            System.out.print("Movie's rating: ");
            System.out.println(ratings[n] + "\n");

        }
    }

    public static void addManyMovies(String[] movies, double[] ratings, int inp) {
        for (int t = 0; t < inp; t++) {
            Scanner sc1 = new Scanner(System.in);
            if (movies[t] == null && inp <= 3) {
                System.out.println("Enter movie name: ");
                movies[t] = sc1.nextLine();
                System.out.println("Enter movie's rating: ");
                ratings[t] = sc1.nextDouble();
            } else {
                System.out.println("Array is full or out of bound");
                break;
            }
        }
    }

    public static void searchByName(String[] movies, double[] ratings, String movieName) {
        for (int q = 0; q < movies.length; q++) {
            if (movies[q].equals(movieName)) {
                System.out.println(ratings[q]);
                break;
            } else if (q == movies.length - 1) {
                System.out.println("Movie not found");
            }
        }
    }

    public static void showMenu() {
        System.out.println();
        System.out.println("Press 1 to input movies");
        System.out.println("Press 2 to display movies and ratings");
        System.out.println("Press 3 to input many movies: ");
        System.out.println("Press 4 to find statistics: ");
        System.out.println("Press 5 to search for a movie: ");
        System.out.println("Press 6 to update a movie rating: ");
        System.out.println("Press 7 to sort the movies: ");
        System.out.println("Press 0 to exit");
    }

    public static void addThreeMovies(String[] movies, double[] ratings) {
        int k = 0;
        while (k < 3) {
            Scanner sc1 = new Scanner(System.in);
            System.out.println("Enter movie name: ");
            movies[k] = sc1.nextLine();
            System.out.println("Enter movie's rating: ");
            ratings[k] = sc1.nextDouble();
            k++;
        }
    }
}